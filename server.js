const express = require("express");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

const app = express();
const PORT = 2000;

const cors = require("cors");

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Import Controllers
const authController = require("./controllers/authController");
const carsController = require("./controllers/carsController");

// Import Midleware
const middlewares = require("./middlewares/auth");

// Define Routes
// Auth
app.post("/auth/register", authController.register);
app.post("/auth/register/admin", middlewares.authenticate, middlewares.isSuperAdmin, authController.register);
app.post("/auth/login", authController.login);
app.get("/auth/me", middlewares.authenticate, authController.currentUser);

app.post("/auth/login-google", authController.loginGoogle);
//Cars
app.get("/cars", middlewares.authenticate, middlewares.isAdminSuperAdmin, carsController.getAll);
app.post("/cars/create", middlewares.authenticate, middlewares.isAdminSuperAdmin, carsController.create);
app.put("/cars/update/:id", middlewares.authenticate, middlewares.isAdminSuperAdmin, carsController.updateCars);
app.delete("/cars/delete/:id", middlewares.authenticate, middlewares.isAdminSuperAdmin, carsController.deletedCars);
app.get("/cars/available?", carsController.getByAvailable);

// API Documentation
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(PORT, () => {
    console.log(`Server berhasil berjalan di port http://localhost:${PORT}`);
});
