const carsService = require("../services/carsService");

const create = async (req, res, next) => {
    const {
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,

    } = req.body;

    const created_by = req.user.name;


    const { status, status_code, message, data } = await carsService.create({
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        created_by,
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

const getAll = async (req, res) => {
    const { status, status_code, message, data } = await carsService.getAll();

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

const updateCars = async (req, res, next) => {
    const { id } = req.params;
    const {
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,

    } = req.body;

    const updated_by = req.user.name;


    const { status, status_code, message, data } = await carsService.updateCars({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

const deletedCars = async (req, res) => {
    const { id } = req.params;

    const deleted_by = req.user.name

    const { status, code_status, message, data } = await carsService.deletedCars({
        id,
        deleted_by
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

const getByAvailable = async (req, res) => {
    const { is_with_driver, capacity, available_at } = req.query;

    const { status, code_status, message, data } = await carsService.getByAvailable({is_with_driver, capacity, available_at});

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

module.exports = { create, getAll, updateCars, deletedCars, getByAvailable };
