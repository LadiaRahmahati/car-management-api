const { cars } = require("../models");
const { Op } = require("sequelize");

class CarsRepository {
    static async create({
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        created_by,
    }) {
        const createdCar = cars.create({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            created_by,
        });

        return createdCar;
    }

    static async getAll({
        is_with_driver,
        capacity,
        available_at
    }) {
        if (is_with_driver && capacity && available_at) {
            console.log("ini", is_with_driver);
            console.log("i", capacity);
            console.log("a", available_at);

            const carsFiltered = await cars.findAll({
                where: {
                    is_with_driver,
                    capacity,
                    available_at: {
                        [Op.lt]: available_at,
                    },

                }
            });

            return carsFiltered;
        }

        return cars;
    }

    static async updateCars({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    }) {
        const updatedCar = cars.update({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            updated_by,
        }, {
            where: {
                id
            }
        });

        return updatedCar;
    }

    static async deletedCars({
        id
    }) {
        const deletedCar = await cars.destroy({
            where: {
                id
            }
        });

        return deletedCar;
    }



}

module.exports = CarsRepository;
