const { users } = require("../models");

class UsersRepository {
    static async getByID({ id }) {
        const getUser = await users.findOne({ where: { id } });

        return getUser;
    }

    static async getByEmail({ email }) {
        const getUser = await users.findOne({ where: { email } });

        return getUser;
    }

    static async create({ name, email, password, role }) {
        const createdUser = users.create({
            name,
            email,
            password,
            role,
        });

        return createdUser;
    }

}

module.exports = UsersRepository;