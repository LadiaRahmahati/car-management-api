const carsRepository = require("../repositories/carsRepository");

class CarsService {
    static async create({
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        created_by,
    }) {
        try {
            if (!plate) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Plate wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!manufacture) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Manufacture wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!model) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Model wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }
            if (!image) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Image wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!rent_per_day) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Rent_per_day wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!capacity) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Capacity wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!description) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Description wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!transmission) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Transmission wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!type) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Type wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!year) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Year wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!options) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Options wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!specs) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Specs wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }
            if (!available_at) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Available_at wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!is_with_driver === null) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Is_with_driver wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            const createdCar = await carsRepository.create({
                plate,
                manufacture,
                model,
                image,
                rent_per_day,
                capacity,
                description,
                transmission,
                type,
                year,
                options,
                specs,
                available_at,
                is_with_driver,
                created_by,
            });

            return {
                status: true,
                status_code: 201,
                message: "Car created successfully",
                data: {
                    created_car: createdCar,
                },
            };
        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    registered_user: null,
                },
            };
        }
    }
    static async getAll() {
        try {
            const getCars = await carsRepository.getAll();

            return {
                status: true,
                status_code: 201,
                message: "Data mobil berhasil ditampilkan",
                data: {
                    data_cars: getCars,
                },
            };
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    data_cars: null,
                },
            };
        }
    }

    static async updateCars({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    }) {
        try {
            if (!plate) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Plate wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!manufacture) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Manufacture wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!model) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Model wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }
            if (!image) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Image wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!rent_per_day) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Rent_per_day wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!capacity) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Capacity wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!description) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Description wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!transmission) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Transmission wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!type) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Type wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!year) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Year wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!options) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Options wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!specs) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Specs wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!is_with_driver === null) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Is_with_driver wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            if (!capacity) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Capacity wajib diisi",
                    data: {
                        created_cars: null,
                    },
                };
            }

            const updatedCar = await carsRepository.updateCars({
                id,
                plate,
                manufacture,
                model,
                image,
                rent_per_day,
                capacity,
                description,
                transmission,
                type,
                year,
                options,
                specs,
                available_at,
                is_with_driver,
                updated_by,
            });

            return {
                status: true,
                status_code: 201,
                message: "Car updated successfully",
                data: {
                    updated_car: updatedCar,
                },
            };
        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    updated_car: null,
                },
            };
        }
    }
    static async deletedCars({ id, deleted_by }) {
        try {
            const deletedCar = await carsRepository.deletedCars({
                id,
                deleted_by
            });

            return {
                status: true,
                code_status: 200,
                message: "Car deleted successfully",
                data: {
                    deleted_car: deletedCar,
                },
            }
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    deleted_car: null,
                },
            };
        }

    }
    static async getByAvailable({ is_with_driver, capacity, available_at }) {
        try {
            const getAvailable = await carsRepository.getAll({ is_with_driver, capacity, available_at});

            return {
                status: true,
                code_status: 200,
                message: "data mobil berhasil ditampilkan",
                data: {
                    carsFiltered: getAvailable,
                },
            };
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    cars: null,
                },
            };
        }
    }

}



module.exports = CarsService;